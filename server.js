var DIRECTORIES_TO_SCAN = 'endpoints',
	API_PATH = '/api/rest/',
	colors = require('colors'),
	_ = require('underscore'),
	fs = require('fs'),
	express = require('express'),
	mysql = require('mysql'),
	Promise = require('promise'),
	crypto = require("crypto"),
	randomstring = require("randomstring"),	
	app = express(),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	endpointDirs = fs.readdirSync(DIRECTORIES_TO_SCAN);

//listo
var conn = mysql.createConnection({
		host     : process.env.npm_package_config_ddbb_host, 
		user     : process.env.npm_package_config_ddbb_user,
		password : process.env.npm_package_config_ddbb_pass,
		port	 : parseInt(process.env.npm_package_config_ddbb_port),
		database : process.env.npm_package_config_ddbb
	});

conn.connect(function(error){
	if (error){
		console.log("Doesn't established connection...");
	} else{
		console.log("Does established connection...");
	}
});

app.ddbb = conn;

/* Adding utilities to generate endpoints */

app.util = {
	handler: function(options) {
		var verb = '[' + options.verb.toUpperCase() + ']';
		console.log('endpoint:', verb.blue.bold, options.endpoint.magenta.underline);
		app[options.verb](options.endpoint, options.callback);
	}
};

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('root/'));

/***************** Security Middleware *****************/
app.use(function(req, res, next){
	var securityToken = req.get("X-token"); 
	
	if (securityToken){
		var sql = "SELECT user_id userId, u.uuid, u.firstName, u.lastName, "
		+"u.password_key passKey, u.isOnce, (SELECT COUNT(*) FROM authors a WHERE "
		+"a.user_id = u.id) isAuthor, st.token, st.token_sep tokenSep, "
		+"st.ip, st.user_agent userAgent FROM security_tokens st JOIN users u ON "
		+"st.user_id = u.id WHERE x_token = ?";

		new Promise(function(success, failed){
			app.ddbb.query(sql, [securityToken], function(err, rows, fields){
				if (err) {
					failed(err);
				}
				success(rows);
			});
		}).then(
			function(rows){
				if (rows.length > 0){
					var userAgent = (req.headers['user-agent']) ? 
						req.headers['user-agent']: '',
						isUnauthorized = false;

					if (rows[0].userAgent != userAgent){
						res.status(401);
						res.json({
							appCode: 1,
							appMessage: "User unauthorized: User agent invalid..."
						}).end();							
					}

					var cipher = crypto.createCipher("aes256", rows[0].passKey),
						_xToken = rows[0].token + rows[0].tokenSep + rows[0].ip,
						xToken = cipher.update(_xToken, 'utf8', 'hex') + cipher.final('hex');

					if (securityToken != xToken){
						res.status(401)
							.json({
								appCode: 1,
								appMessage: "User unauthorized: Toker invalid..."
							}).end();					
					}

					req.isAdmin = !rows[0].isAuthor;

					if (req.isAdmin && (req.path.includes("like") || (req.path.includes("comments") && req.method == 'POST'))){
						res.status(401)
							.json({
								appCode: 1,
								appMessage: "User unauthorized: 1..."
							}).end();
					}

					if (!req.isAdmin && req.path.includes("admin")){
						res.status(401)
							.json({
								appCode: 1,
								appMessage: "User unauthorized: 2..."
							}).end();		
					}
					req.userId = rows[0].userId;
					req.userUuid = rows[0].uuid;
					req.firstName = rows[0].firstName;
					req.lastName = rows[0].lastName;
					next();	
				}
				else{
					res.status(401)
						.json({
							appCode: 1,
							appMessage: "User unauthorized : 3..."
						}).end();		
				}
			},
			function(err){
				res.status(500);
				res.send("Server error...");
			}
		);
	}
	else{
		if (req.path.includes("admin") || 
			(req.path.includes("all") && (
				req.path.includes("like")  || req.path.includes("unlike")
				)) ||			
			req.path.includes("own") || 
			req.path.includes("readers") ||
			req.path.includes("users")) {

			res.status(401);
			res.json({
				appCode: 1,
				appMessage: "User unauthorized..."
			}).end();	

		}
		next();
	}
});
/* Load endpoint definitions by scanning express/endpoints directory */

_.forEach(endpointDirs, function(endpointDir) {
	require('./' + DIRECTORIES_TO_SCAN + '/' + endpointDir + '/endpoint.js')(app, API_PATH);
});

app.listen(process.env.npm_package_config_server_port, function(){
	console.log("Ready in "+process.env.npm_package_config_server_port+"!");
});